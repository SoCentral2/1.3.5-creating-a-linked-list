//
//  LinkedList.hpp
//  CommandLineTool
//
//  Created by Paul Rathbone on 15/04/2020.
//  Copyright © 2020 Tom Mitchell. All rights reserved.
//
//LinkedList - Manages a linked list of floats
//With help from https://www.codesdope.com/blog/article/linked-list-traversal-using-loop-and-recursion-in-/

#ifndef LinkedList_hpp
#define LinkedList_hpp
#include <stdio.h>

class LinkedList
{
public:
    LinkedList();
    ~LinkedList();
    void add (float itemValue);
    float get (int index);
    int size () const;
    
private:
    struct Node
    {
        float value = 0;
        Node* next;
    }*nodeHead, *nodeTail;
};
#endif /* LinkedList_hpp */

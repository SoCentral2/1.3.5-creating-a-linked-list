//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>
#include "LinkedList.hpp"

int max (int a, int b)
{
    if (a<b)
        return b;
    else
        return a;
}

double max (double a, double b)
{
    std::cout << "\nmax for doubles\n";
    if (a<b)
        return b;
    else
        return a;
}

template <typename Type>
Type min (Type a, Type b)
{
    if (a < b)
        return a;
    else
        return b;
}

template <typename Type>
Type add (Type a, Type b)
{
    return a + b;
}

template <typename Type>
void print (Type *myArray, int intSize)
{
    for (int i = 0; i < intSize; i++)
    {
        std::cout << "\nElement " << i << " = " << myArray[i];
    }
    std::cout << "\n";
}

template <typename Type>
double getAverage (Type *myArray, int intSize)
{
    Type runningTotal = 0;
    for (int i = 0; i < intSize; i++)
    {
        runningTotal += myArray[i];
    }
    return runningTotal/intSize;
}

int main()
{
    int myArray[5]{2, 4, 6, 8, 10};
    print(myArray, 5);
    std::cout << "Avg: " << getAverage(myArray, 5) << "\n";
    
    float mydblArray[5]{.0550, 4.33, 6.3333, 8.7, 10.1010101};
    print(mydblArray, 5);
    std::cout << "Avg: " << getAverage(mydblArray, 5) << "\n";
    
    /*
    LinkedList myLinkedList;
    
    myLinkedList.add(5.0);
    myLinkedList.add(10.3);
    myLinkedList.add(15.85);
    myLinkedList.add(94.7);

    std::cout << "\n";
    std::cout << "\n" << myLinkedList.get(0);
    std::cout << "\n" << myLinkedList.get(1);
    std::cout << "\n" << myLinkedList.size();
    std::cout << "\n";
    return 0;
     */
}

//
//  LinkedList.cpp
//  CommandLineTool
//
//  Created by Paul Rathbone on 15/04/2020.
//  Copyright © 2020 Tom Mitchell. All rights reserved.
//

#include "LinkedList.hpp"

LinkedList::LinkedList()
{
    nodeHead = nullptr;
    nodeTail = nullptr;
}

LinkedList::~LinkedList()
{
    Node* nodeNext = new Node;
    Node* nodeCurrent = new Node;
    nodeCurrent = nodeHead;
    nodeNext = nodeHead->next;
    while (nodeNext != nullptr)
    {
        delete nodeCurrent;
        nodeCurrent = nodeNext;
        nodeNext = nodeCurrent->next;
    }
    delete nodeCurrent;
}

void LinkedList::add (float itemValue)
{
    Node *nodeTemp = nullptr; //Create pointer
    nodeTemp = new Node; //Point it to heap
    
    nodeTemp->value = itemValue;
    nodeTemp->next = nullptr;
    
    if (nodeHead == nullptr)
    {
        nodeHead = nodeTemp;
        nodeTail = nodeTemp;
    }
    else
    {
        nodeTail->next = nodeTemp;
        nodeTail = nodeTail->next;
    }
}

float LinkedList::get (int index)
{
    Node *nodeTemp = nullptr; //Create pointer
    nodeTemp = new Node; //Point it to heap
    
    nodeTemp = nodeHead;
    for (int i = 0; i < index; i++)
    {
        nodeTemp = nodeTemp->next;
    }
    
    return nodeTemp->value;
}

int LinkedList::size () const
{
    int intCount = 0;
    Node *nodeTemp = nullptr; //Create pointer
    nodeTemp = new Node; //Point it to heap
    nodeTemp = nodeHead;
    while (nodeTemp != nullptr)
    {
        nodeTemp = nodeTemp->next;
        intCount++;
    }
    return intCount++;
}
